package interpreter.debugger;

import java.util.Iterator;
import java.util.Set;

/**
 * This is based on the lexer's SymbolTable object, it was modifed to take String's rather
 * then Symbol's for the key-value pair.
 * @author frank
 */
class Binder {

    /**
     * <
     * pre>
     *  Binder objects group 3 fields
     *  1. a value
     *  2. the next link in the chain of symbols in the current scope
     *  3. the next link of a previous Binder for the same identifier
     *     in a previous scope
     * </pre>
     */

    private Object value;
    private String prevtop;   // prior symbol in same scope
    private Binder tail;      // prior binder for same symbol
    // restore this when closing scope

    Binder(Object v, String p, Binder t) {
        value = v;
        prevtop = p;
        tail = t;
    }

    Object getValue() {
        return value;
    }

    String getPrevtop() {
        return prevtop;
    }

    Binder getTail() {
        return tail;
    }
}

/**
 *
 * @author frank
 */
/**
 * <pre>
 * The SymbolTable class is similar to java.util.Dictionary, except that
 * each key must be a Symbol and there is a scope mechanism.

 * </pre>
 */
public class SymbolTable {

    private java.util.HashMap<String, Binder> symbols = new java.util.HashMap<String, Binder>();
    private java.util.Stack<String> prevKeys = new java.util.Stack<String>();
    private String top;    // reference to last symbol added to
    // current scope; this essentially is the
    // start of a linked list of symbols in scope
    private Binder marks;  // scope mark; essentially we have a stack of
    // marks - push for new scope; pop when closing
    // scope

    /*
     public static void main(String args[]) {
     Symbol s = Symbol.symbol("a", 1),
     s1 = Symbol.symbol("b", 2),
     s2 = Symbol.symbol("c", 3);

     Table t = new Table();
     t.beginScope();
     t.put(s,"top-level a");
     t.put(s1,"top-level b");
     t.beginScope();
     t.put(s2,"second-level c");
     t.put(s,"second-level a");
     t.endScope();
     t.put(s2,"top-level c");
     t.endScope();
     }

     */
    public SymbolTable() {
    }

    /**
     * <pre>Overloaded output toString to return a formatted string of the key/val pairs.
     * @return formatted output string. </pre>
     */
    @Override
    public String toString() {
        String returnVal = "";
        Set<String> keys = symbols.keySet();
        Iterator<String> iterator = keys.iterator();

        //Get all key/value pairs in the symTable 
        while (iterator.hasNext())
        {
            String element = iterator.next();
            Binder bind = symbols.get(element);
            returnVal += element + "/" + bind.getValue();
            if (iterator.hasNext())
            {
                returnVal += ",";
            }
        }
        
        //Return the formatted string
        return returnVal;
    }

    /**
     * Gets the object associated with the specified symbol in the Table.
     */
    public Object get(String key) {
        Binder e = symbols.get(key);
        return e.getValue();
    }

    /**
     * Puts the specified value into the Table, bound to the specified
     * Symbol.<br>
     * Maintain the list of symbols in the current scope (top);<br>
     * Add to list of symbols in prior scope with the same string identifier
     */
    public void put(String key, Object value) {
        symbols.put(key, new Binder(value, top, symbols.get(key)));
        prevKeys.push(key); //Save the key so we can later pop them out
        top = key;
    }

    /**
     * Remembers the current state of the Table; push new mark on mark stack
     */
    public void beginScope() {
        marks = new Binder(null, top, marks);
        top = null;
    }

    /**
     * Restores the table to what it was at the most recent beginScope that has
     * not already been ended.
     */
    public void endScope() {
        while (top != null)
        {
            Binder e = symbols.get(top);
            if (e.getTail() != null)
            {
                symbols.put(top, e.getTail());
            }
            else
            {
                symbols.remove(top);
            }
            top = e.getPrevtop();
        }
        top = marks.getPrevtop();
        marks = marks.getTail();
    }

    public void pop(int n) {
        //Pop n items off our prevKey stack and remove them from the symTable as well.
        for (int i = 0; i < n; i++)
        {
            //Retrieve the last key put in the symbol table
            String key = prevKeys.pop();
            Binder bind = symbols.get(key);
            if (bind.getTail() != null)
            {
                //We have previous values for this key pair, so restore them.
                Binder oldBind = bind.getTail();
                symbols.remove(key);
                symbols.put(key, oldBind); //Restore old binder
            }
            else
            {
                //No previous values, just remove the key pair
                symbols.remove(key);
            }
        }
    }

    /**
     * @return a set of the Table's symbols.
     */
    public java.util.Set<String> keys() {
        return symbols.keySet();
    }
}
