//FunctionEnvironmentRecord keeps track of the current func, start and end line,
//current line and the SymbolTable which holds key/val pairs.


package interpreter.debugger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * <pre> FunctionEnvironmentRecord holds the SymbolTable, start and end line of the current
 * executing function, the currentLine execution is on in source file and the function name. 
 * A FunctionEnvironmentRecord is generated for each DebugCallByteCode encountered.</pre>
 * @author frank
 */
public class FunctionEnvironmentRecord {

    private int startLine; //Start of current function in source code
    private int endLine;   //Stop of current function in source code
    private int currentLine; //Current line of execution in source code
    private String name;    //Name of current function
    
    private SymbolTable symTable; //SymbolTable holds key and offset pairs of variables for debugging
    
    /**
     * <pre>Unit testing of FunctionEnvironmentRecord, opens file passed into command line
     *      It then executes the basic instructions.
     *      BS
     *      Function g 1 20
     *      Line 5
     *      Enter a 4
     *      Enter b 2
     *      Enter c 7
     *      Enter a 1
     *      Pop 2
     *      Pop 1
     * </pre>
     * @param args 
     */
    public static void main(String[] args)
    {
        try
        {
            FunctionEnvironmentRecord fctEnvRecord = new FunctionEnvironmentRecord();
            
            String programFile = args[0]; //Retrieve filename
            //Unit test here
            BufferedReader source;
            source = new BufferedReader(new FileReader(programFile));
            
            String currString;
            do {
                //Read the next line
                currString = source.readLine();
                

                //source.readLine() 
                if (currString != null) {
                    //Break our line into space seperate tokens
                    String[] splitString = currString.split("\\s");

                    
                    if (splitString.length <= 0) {
                        ; //Ignore empty lines
                    } 
                    else {
                        if (splitString[0].equals("BS")) {
                            //Begin Scope
                            fctEnvRecord.beginScope();
                        }
                        
                        else if (splitString[0].equals("Function"))
                        {
                            //Function g 1 20
                            String funcName = splitString[1];
                            String start = splitString[2];
                            String stop = splitString[3];
                            int startInt = Integer.parseInt(start);
                            int stopInt = Integer.parseInt(stop);
                            fctEnvRecord.setFunction(funcName, startInt, stopInt);
                            
                            
                        }
                        else if (splitString[0].equals("Line"))
                        {
                            //Line 3
                            String lineNum = splitString[1];
                            int lineNumInt = Integer.parseInt(lineNum);
                            fctEnvRecord.setCurrentLine(lineNumInt);
                        }
                        else if (splitString[0].equals("Enter"))
                        {
                            //Enter a 4
                            String key = splitString[1];
                            String val = splitString[2];
                            int valInt = Integer.parseInt(val);
                            fctEnvRecord.enterSym(key, valInt);
                        }
                        else if (splitString[0].equals("Pop"))
                        {
                            String n = splitString[1];
                            int nInt = Integer.parseInt(n);
                            fctEnvRecord.pop(nInt);
                        }
                        System.out.println(String.format("%-20s     %s", currString, fctEnvRecord.toString()));
                    }

                   
                }
            } while (currString != null);
        } catch (FileNotFoundException ex)
        {
            System.out.println("File not Found!");
            System.exit(1);
        } catch (IOException ex)
        {
            System.out.println("Couldn't read new line!");
            System.exit(1);
        }
        
    }

    /**
     * Retrieve the symbol table
     * @return symTable
     */
    public SymbolTable getSymbolTable()
    {
        return this.symTable;
    }
    
    
    /**
     * Get the start line of this environment record
     * @return startLine
     */
    public int getStartLine()
    {
        return this.startLine;
    }
    
    /**
     * Get the stop line of this environment record
     * @return endLine
     */
    public int getStopLine()
    {
        return this.endLine;
    }
    
    /**
     * Get the record's function name
     * @return 
     */
    public String getFunctionName()
    {
        //We need to strip the <<2>> ending on some of the functions
        //An easy way to do this is to split with a regexp and then only
        //return the first array entry.
        String funcName = name;
        String[] splitFuncName = name.split("<");
        
        return splitFuncName[0];
    }
    
    /**
     * <pre>Overloaded string that returns a function name, startLine, endLine, and currentLine.</pre>
     * @return 
     */
    @Override
    public String toString() {
        
        //-999 is sentinel value to inficate uninitialized FunctionEnvironmentRecord's
        String output = "(<" + symTable + ">,  " + name + ", ";
        if (startLine == -999)
        {
            output += "-, ";
        }
        else
        {
            output += startLine + ", ";
        }
        
        if (endLine == -999)
        {
            output += "-, ";
        }
        else
        {
            output += endLine + ", ";
        }
        
        if (currentLine == -999)
        {
            output += "-)";
        }
        else
        {
            output += currentLine + ")";
        }
        
        return output;
    }
    
    /**
     * <pre>Default constructor, sets up the FunctionEnvironmentRecord</pre> 
     */
    public FunctionEnvironmentRecord()
    {
        //Create SymbolTable
        symTable = new SymbolTable();
        
        //Initialize internal variables to sentinel value for easy viewing
        startLine = -999;
        endLine = -999;
        currentLine = -999;
        name = "-";
    }
    
    /**
     * <pre>Sets the function name and the start/stop line number.</pre>
     * @param name
     * @param start
     * @param stop 
     */
    public void setFunction(String name, int start, int stop)
    {
        //Set current funtion information that is passed in.
        this.name = name;
        this.startLine = start;
        this.endLine = stop;
    }
    
    /**
     * <pre>Sets the current line of execution</pre>
     * @param line 
     */
    public void setCurrentLine(int line)
    {
        //Set currentLine
        this.currentLine = line;
    }
    
    /**
     * Set the start line
     * @param line 
     */
    public void setStartLine(int line)
    {
        this.startLine = line;
    }
    
    /**
     * Set the end line
     * @param line 
     */
    public void setEndLine(int line)
    {
        this.endLine = line;
    }
    
    /**
     * <pre>Begin a new scope</pre>
     */
    public void beginScope()
    {
        //Tell SymbolTable to begin a new scope
        symTable.beginScope();
    }
    
    /**
     * <pre>Enter symbol and offset</pre>
     * @param sym
     * @param offset 
     */
    public void enterSym(String sym, int offset)
    {
        //Put String into SymbolTable
        symTable.put(sym, offset);
    }
    
    /**
     * <pre>Pop n items off the SybolTable</pre>
     * @param n 
     */
    public void pop(int n)
    {
        //Pop n items off the SymbolTable
        symTable.pop(n);
    }
    
    /**
     * <pre>Get current execution line</pre>
     * @return 
     */
    public int getCurrentLine()
    {
        return currentLine;
    }
}
