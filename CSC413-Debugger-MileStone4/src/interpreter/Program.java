package interpreter;

import interpreter.bytecodes.ByteCode;
import interpreter.bytecodes.CallCode;
import interpreter.bytecodes.FalseBranchCode;
import interpreter.bytecodes.GotoCode;
import interpreter.bytecodes.LabelCode;
import interpreter.bytecodes.debuggerByteCodes.DebugCallCode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Program object maintains a list of ByteCode's, it also resolves the symbolic
 * addresses
 *
 *
 * @author Frank Dye
 */
public class Program {

    //We need a dynamic array of ByteCode to store each individual bytecode
    protected List<ByteCode> byteCodeList;

    //We also need a map that implements a one to many relationship
    //it will take a label <A> and store the ByteCode objects that references that label
    //as we process each line. Then when we hit LABEL <A> it will query the map and resolve the 
    //address of each object the map references. This keeps us from having to do multi-pass
    //processing of the file.
    //I.E. Map<String,List<ByteCode>> labelMap = new HashMap<String,List<ByteCode>>();
    protected Map<String, List<ByteCode>> labelToObjectMap;

    //The labelMap associates each label with its symbolic address for later resolution
    protected Map<String, Integer> labelToAddressMap;

    protected int currSymbolicAddress;

    /**
     * Default constructor, sets up our internal objects and sets
     * currSymbolicAddress to 0
     */
    public Program() {
        //Initialization code
        byteCodeList = new ArrayList<>();
        labelToObjectMap = new HashMap<>();
        labelToAddressMap = new HashMap<>();

        currSymbolicAddress = 0;
    }

    /**
     * Add ByteCode to the internal byteCodeList and stores symbolicAddress's
     * into internal data structures for later resolution.
     *
     * @param bytecode
     */
    public void addByteCode(ByteCode bytecode) {
        String byteCodeName = bytecode.getByteCodeName();

        //GOTO, FALSEBRANCH, and CALL all need to have labels resolves so put them into labelMap
        //our one to many data structure
        if (       byteCodeName.equals("GotoCode")
                || byteCodeName.equals("FalseBranchCode")
                || byteCodeName.equals("CallCode")
                || byteCodeName.equals("DebugCallCode"))
        {
            String label = null;
            if (byteCodeName.equals("GotoCode"))
            {
                GotoCode gotoByteCode = (GotoCode) bytecode; //Case the ByteCode
                label = gotoByteCode.getAddress();
            }
            else if (byteCodeName.equalsIgnoreCase("FalseBranchCode"))
            {
                FalseBranchCode falseBranchCode = (FalseBranchCode) bytecode; //cast bytecode
                label = falseBranchCode.getAddress();
            }
            else if (byteCodeName.equals("CallCode"))
            {
                CallCode callCode = (CallCode) bytecode; //Cast bytecode
                label = callCode.getAddress();
            }
            else if (byteCodeName.equals("DebugCallCode"))
            {
                CallCode callCode = (DebugCallCode) bytecode; //Cast bytecode
                label = callCode.getAddress();
            }
            if (labelToObjectMap.containsKey(label))
            {
                //Label already exists so just append this ByteCode object to the existing List
                List byteCodeLabelList = labelToObjectMap.get(label);
                
                byteCodeLabelList.add(bytecode);
            }
            else
            {
                //Label has not been inserted yet so create a List object, add the bytecode to it
                //then insert it in our Map with the correct key
                List<ByteCode> byteCodeLabelList = new ArrayList<>();
                byteCodeLabelList.add(bytecode);
                
                //Insert into labelMap
                labelToObjectMap.put(label, byteCodeLabelList);
            }
        }
        else if (byteCodeName.equals("LabelCode"))
        {
            String label = null;
            LabelCode labelCode = (LabelCode) bytecode;
            label = labelCode.getLabel();
            if (labelToAddressMap.containsKey(label))
            {
                //Duplicate label found, exit with error msg
                System.out.println("***Duplicate label found, invalid bytecode!");
                System.exit(1);
            }
            else
            {
                //Add our label with its symbolic address to labelToAddressMap
                labelToAddressMap.put(label, currSymbolicAddress);
            }

        }

        //General code, always execute
        //Add the bytecode to byteCodeList, increment currSymbolicAddress
        byteCodeList.add(bytecode);
        currSymbolicAddress++;

    }

    /**
     * Dump the Program to stdout, used for Debug
     */
    public void printProgram() {
        System.out.println("\nProgramDump: \n");
        for (ByteCode bc : byteCodeList)
        {

            System.out.println(bc + "\n");
        }
    }

    /**
     * Resolve the symbolic address's used by LABEL, GOTO, FALSEBRANCH, CALL We
     * cycle through each label and get each call to it. We then replace the
     * label symbolic address with the physical address.
     */
    public void resolveAddress() {
        for (String label : labelToAddressMap.keySet())
        {
            //For each label, first retrieve the symbolic address
            int symbolicAddress = labelToAddressMap.get(label);

            //Now retrieve the list of ByteCode objects that branch to this label
            List branchByteCodes = labelToObjectMap.get(label);

            //It is possible that a LABEL is never branched too, that would mean
            //we might not find a GOTO, etc to link to
            //In that case, ignore this label
            if (branchByteCodes != null)
            {
                for (Iterator it = branchByteCodes.iterator(); it.hasNext();)
                {
                    //Change each ByteCodes first argument which is the label
                    //to the correct symbolicAddress
                    ByteCode bc = (ByteCode) it.next();
                    
                    String byteCodeName = bc.getByteCodeName();
                    
                    if (byteCodeName.equals("GotoCode"))
                    {
                        GotoCode gotoCode = (GotoCode) bc;
                        gotoCode.setAddress(Integer.toString(symbolicAddress));
                    }
                    else if (byteCodeName.equals("FalseBranchCode"))
                    {
                        FalseBranchCode falseBranchCode = (FalseBranchCode) bc;
                        falseBranchCode.setAddress(Integer.toString(symbolicAddress));
                    }
                    else if (byteCodeName.equals("CallCode"))
                    {
                        CallCode callCode = (CallCode) bc;
                        callCode.setAddress(Integer.toString(symbolicAddress));
                    }  
                    else if (byteCodeName.equals("DebugCallCode"))
                    {
                        CallCode callCode = (DebugCallCode) bc;
                        callCode.setAddress(Integer.toString(symbolicAddress));
                    }  
                }
            }
        }
    }

    /**
     * Return the ByteCode saved at addr
     * @param addr
     * @return ByteCode at addr
     * @throws ByteCodeSyntaxErrorException 
     */
    public ByteCode requestByteCodeAtAddr(int addr) throws ByteCodeSyntaxErrorException {
        if (addr >= 0 && addr < byteCodeList.size())
        {
            //Check to make sure the address is greater then zero and within the program size
            return byteCodeList.get(addr);

        }
        else
        {
            throw new ByteCodeSyntaxErrorException("Tried to access ByteCode out of range!");
        }
    }
}
