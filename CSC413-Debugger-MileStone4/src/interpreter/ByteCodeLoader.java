/* This class handles opening the bytecode file, parsing each line, then creating
 the program object that is handed to the virtual machine
 */
package interpreter;

import interpreter.bytecodes.ByteCode;
import java.io.*;
import java.util.Vector;

/**
 *
 * @author Frank Dye
 */
public class ByteCodeLoader {

    protected Program program;
    protected BufferedReader source;
    protected String currString;
    protected int lineNum;

    /**
     * Performs basic initialization and opening of programFile for reading line
     * by line.
     *
     * @param programFile to open
     * @throws IOException
     */
    public ByteCodeLoader(String programFile) throws IOException {
        //General initialization
        lineNum = 1;
        program = new Program();
        currString = "";

        //Open the sourceFile
        source = new BufferedReader(new FileReader(programFile));

    }

    /**
     * loadCodes() processes each line of the programFile and generates the
     * appropriate ByteCode object. It then adds this to the Program.
     *
     * @return Program
     */
    @SuppressWarnings("empty-statement")
    public Program loadCodes() {
        try {
            do {
                //Read the next line
                currString = source.readLine();
                
                String sourceByteCode = currString;

                //source.readLine() 
                if (currString != null) {
                    //Break our line into space seperate tokens
                    String[] splitString = currString.split("\\s");

                    Vector<String> args = new Vector();
                    if (splitString.length <= 0) {
                        ; //Ignore empty lines
                    } else {
                        if (CodeTable.get(splitString[0]) != null) {
                            String codeTok = splitString[0];
                            //Valid ByteCode token so create the object
                            String codeClass = CodeTable.get(codeTok);

                            ByteCode bytecode = (ByteCode) (Class.forName("interpreter.bytecodes." + codeClass).newInstance());

                            //Add args to bytecode
                            for (int i = 1; i < splitString.length; i++) {
                                args.add(splitString[i]);
                            }
                            
                            //Init the byteCode with its arguments                            
                            bytecode.init(args);
                            
                            //Add in original sourceByteCode for debug, i.e. "LIT 0 m"
                            bytecode.setSourceByteCode(sourceByteCode);
                            
                            program.addByteCode(bytecode);
                        }
                    }

                    //Increment line number
                    lineNum++;
                }
            } while (currString != null);

        } catch (IOException e) {
            System.out.println("***Problem in ByteCodeLoader at lineNum: " + lineNum);
            System.exit(1);
        } catch (ClassNotFoundException ex) {
            System.out.println("***Problem instantiating class, ClassNotFoundException at lineNum: " + lineNum);
            System.exit(1);
        } catch (InstantiationException ex) {
            System.out.println("***Problem instantiating class, InstantiationException at lineNum: " + lineNum);
            System.exit(1);
        } catch (IllegalAccessException ex) {
            System.out.println("***Problem instantiating class, IllegalAccessException at lineNum: " + lineNum);
            System.exit(1);
        }

        program.resolveAddress(); //Resolve symbolic addresses 

        return program;
    }
}
