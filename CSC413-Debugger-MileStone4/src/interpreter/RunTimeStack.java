/**
 * The RunTimeStack class maintains the stack of active frames; when we call a
 * function we'll push a new frame on the stack; when we return from a function
 * we'll pop the top frame
 */
package interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/**
 *
 * @author Frank Dye
 */
public class RunTimeStack {

    //A stack of FrameObject's an association class that maintains previous pc and pointer
    //to addr to RETURN from.
    private Stack<Integer> framePointers;
    private Vector runStack;
   

    private int runStackPtr;

    /**
     * Default constructor, sets up RunTimeStack.
     */
    public RunTimeStack() {
        //Initialize the runStack and framePointers
        framePointers = new Stack<Integer>();
        runStack = new Vector<Integer>();
        
        
        //Set our runStackPtr to 0
        runStackPtr = 0;
    }

    /**
     * Helper function that returns a List of the arguments on the top of the
     * runStack
     *
     * @return List containing args
     */
    public List getArgs() {
        List returnList = new ArrayList();

        int startIndex = 0;
        int stopIndex = runStackPtr;

        if (framePointers.size() != 0)
        {
            //Peek at the top frame
            int nextFrame = framePointers.peek();
            startIndex = nextFrame + 1;
        }
        
        for (int i = startIndex; i < stopIndex; i++)
        {
            returnList.add(runStack.get(i));
        }
        
        return returnList;

    }

    /**
     * Dump the RunTimeStack information for debugging
     */
    public void dump() {
        int numFrame = framePointers.size();

        //If we have no frames saved then just print the runStack
        if (numFrame == 0)
        {
            printDumpHelper(0, runStackPtr - 1);
        }
        //Else print from the bottom to the next frame, and then each subsequent frame
        else
        {
            int startIndex = 0;
            for (int i = 0; i < numFrame; i++)
            {
                int nextFrame = framePointers.get(i);

                int rStackPtr = nextFrame;
                printDumpHelper(startIndex, rStackPtr);
                startIndex = rStackPtr + 1;

            }

            //Print last frame
            printDumpHelper(startIndex, runStackPtr - 1);

        }

    }

    /**
     * Helper function to print a stack dump of runStack i.e. [0,0,0]
     *
     * @param start
     * @param stop
     */
    private void printDumpHelper(int start, int stop) {
        System.out.print('[');
        for (int i = start; i <= stop; i++)
        {
            System.out.print(runStack.get(i));
            //Dont print the comma after the last entry
            if (i != (stop))
            {
                System.out.print(',');
            }
        }
        System.out.print(']');
    }

    /**
     * Peek at the top item without removing it
     *
     * @return the top item of the stack
     */
    public int peek() {
        return (int) runStack.get(runStackPtr - 1);
    }

    /**
     * Push param i on stack
     *
     * @param i push this item on stack
     * @return the item just pushed
     */
    public int push(int i) {

        runStack.add(runStackPtr, i);
        runStackPtr++;
        return i;
    }

    /**
     * Pops the top of the runtime stack
     *
     * @return value
     */
    public int pop() {
        //Pop the 
        int popVal = (int) runStack.get(runStackPtr - 1);
        runStack.set(runStackPtr - 1, 0);
        runStackPtr--;
        return popVal;
    }

    /**
     * Start new frame
     *
     * @param offset indicates the number of slots down from the top of the
     * stack to start the new frame
     */
    public void newFrameAt(int offset) {
        //Save runStackPtr
        //runStackPtr -1 adjusts for starting from 0 index
        // - offset adjusts the frame correctly
        int rStackPtr = (runStackPtr - 1) - offset;

        
        
        framePointers.add(rStackPtr);

    }

    /**
     * We pop the top of the stack when we return from a frame, before popping
     * we save the top of the stack as the functions return value, we then pop
     * the top frame, and push the return value
     */
    public void popFrame() {
        //Pop the top value 
        int popVal = this.pop();

        //Get the location of the previous frame
        int frObj = framePointers.pop();
        int rStackPtr = frObj;
      
        //Set the current runStackPtr to the old frame
        this.runStackPtr = (rStackPtr + 1); //runStackPtr should point to the next available slot after
        //the old frame.

        //Push the returned value from the previous function
        this.push(popVal);
    }

    /**
     * Used to store into variables
     *
     * @param offset
     * @return
     */
    public int store(int offset) {
        //Change value directly in runStack
        //Pop the top of the stack and then store it at offset position from runStackPtr
        //If we have no saved framePointer then set the current frame to zero

        int frame = 0;

        if (framePointers.size() != 0)
        {
            int frObj = framePointers.peek();
            frame = frObj;
        }
        else
        {
            frame = 0; //There are no saved framePointers so return 0
        }

        //Pop the top of the stack
        int topVal = this.pop();

        //Store this value in frame + offset slot in runStack
        runStack.set(frame + offset, topVal);

        return topVal;
    }

    
    public int getValAtOffset(int offset)
    {
        //Get the current frame ptr, if framePointers.size() == 0 then set frame to 0
        int frame = 0;

        if (framePointers.size() == 0)
        {
            frame = 0;
        }
        else
        {
            //We have frames saved so get the address of the top frame
            int frObj = framePointers.peek();

            //+1 adjusts for index 0
            frame = frObj + 1;
        }

        //Retrieve variable value and push it onto the top of stack
        int retrievedVal = (int) runStack.get(frame + offset);
        
        return retrievedVal;
    }
    
    public int getCurrentStackPtr()
    {
        return this.runStackPtr;
    }
    
    /**
     * Used to load variables onto the stack
     *
     * @param offset
     * @return
     */
    public int load(int offset) {
        //Get the current frame ptr, if framePointers.size() == 0 then set frame to 0
        int frame = 0;

        if (framePointers.size() == 0)
        {
            frame = 0;
        }
        else
        {
            //We have frames saved so get the address of the top frame
            int frObj = framePointers.peek();

            //+1 adjusts for index 0
            frame = frObj + 1;
        }

        //Retrieve variable value and push it onto the top of stack
        int retrievedVal = (int) runStack.get(frame + offset);
        this.push(retrievedVal);
        return retrievedVal;
    }

    /**
     * Used to load literals onto the stack - e.g. for lit 5 we'll call push
     * with 5
     *
     * @param i
     * @return
     */
    public Integer push(Integer i) {
        runStack.add(i);
        runStackPtr++;
        return i;
    }
    
    
    
}
