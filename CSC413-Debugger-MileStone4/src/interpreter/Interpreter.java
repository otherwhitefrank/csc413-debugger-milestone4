/*
 The interpreter parses the command line and passes it to the ByteCodeLoader, it then executes the returned
Program object line for the line, managing the flow of execution and the RunTimeStack
 */

package interpreter;

import interpreter.debugger.DebugByteCodeLoader;
import interpreter.debugger.DebugCodeTable;
import interpreter.debugger.ui.UserInterface;
import java.io.*;
import java.util.Set;

/**
 * Interpreter run the interpreter:
 * 1. Perform all initialization
 * 2. Load the bytecodes from file
 * 3. Run the virtual machine
 * 
 * @author Frank Dye
 */
public class Interpreter
{
    ByteCodeLoader bcl;
    
    /**
     * Open a ByteCodeLoader and gets the CodeTable ready for use
     * @param codeFile 
     */
    public Interpreter(String codeFile) {
        try {
            CodeTable.init(); //Initialize the CodeTable
            
            bcl = new ByteCodeLoader(codeFile);
        }
        catch (IOException e)
        {
            System.out.println("***IOException cannot find file: " + codeFile);
            System.exit(1);
        }
    }
    
    /**
     * Loads codes from ByteCodeLoader into Program object, then initializes 
     * the vm and begins execution.
     */
    void run()
    {
        Program program = bcl.loadCodes();
        VirtualMachine vm = new VirtualMachine(program);
        vm.executeProgram();
    }
    
    /**
     * Takes as input the file to open, creates new Interpreter object and executes run()
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        String byteCodeFileName = "";
        String sourceCodeFileName = "";
        
        boolean debugSet = false;
        
        if (args.length == 0)
        {
            System.out.println("***Incorrect usage, try: java -jar interpreter.jar <file>");
            System.out.println("***Or: java -jar interpreter.jar -d <file no extension>");
            System.exit(1);
        }
        
        //Check for -d flag to enable debugging
        debugSet = args[0].equalsIgnoreCase("-d");
        
        if (debugSet)
        {
            try {
                //Debug flag found, continue in debug mode.
                //Second parameter will be in form "file" so make byteCodeFileName == "file.x.cod"
                //and sourceCodeFileName == "file.x"
                byteCodeFileName = args[1] + ".x.cod";
                sourceCodeFileName = args[1] + ".x";
                
                //Spawn UserInterface which then spawns DebugVirtualMachine and loads the ByteCode.
                CodeTable.init(); //Initialize the original CodeTable
                DebugCodeTable.init(); //Initialize the CodeTable
                DebugByteCodeLoader bcl = new DebugByteCodeLoader(byteCodeFileName);
                Program program = bcl.loadCodes();
                
                //Retrieve validBreakPoints
                Set<Integer> validBreakPoints = bcl.getValidBreakpointLines();
                
                //Create UserInterface passing into it
                //Name of ByteCode File
                //Name
                new UserInterface(program, sourceCodeFileName, validBreakPoints).run();
            } catch (IOException ex) {
                System.out.println("***IOException: can't open " + byteCodeFileName);
                System.exit(1);
            }
        }
        else
        {
            //No debug set, first parameter will be in form "file.x" so don't modify it.
            byteCodeFileName = args[0];
            
            (new Interpreter(byteCodeFileName)).run(); //Start interpreter as normal.
        }
        
        
        
    }
    
}
