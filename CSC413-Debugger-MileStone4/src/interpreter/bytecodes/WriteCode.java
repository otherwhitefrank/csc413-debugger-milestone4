package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * WRITE ByteCode, takes 0 args
 * @author frank
 */
public class WriteCode extends ByteCode {

    public WriteCode() {
        super("WriteCode");
    }
    
    /**
     * Write the value on the top of the stack to standard output
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        
        vm.writeInt();
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "WRITE"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
        ;//No args for WriteCode
    }
}
