package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * LABEL ByteCode, takes 1 argument
 * @author frank
 */
public class LabelCode extends ByteCode {

    private String label;
    
    public LabelCode() {
        super("LabelCode");
    }
    
    public String getLabel()
    {
        return this.label;
    }
    
    public void setLabel(String inLabel)
    {
        this.label = inLabel;
    }
    
    /**
     * Place holder ByteCode to allow FALSEBRANCH, GOTO, CALL 
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Label is only here as a place holder to branch to, no execution needed
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "LABEL continue<<2>>"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
    
        if (!args.isEmpty())
        {
            //Store the first argument as label
            this.label = args.get(0);
        }
    }
}
