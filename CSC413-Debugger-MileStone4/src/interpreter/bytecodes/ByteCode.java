package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * Parent class ByteCode used by all of the ByteCode children
 * execute() and printDebugLine() must be overloaded in all children.
 * @author Frank Dye
 */
public abstract class ByteCode {

    
    protected String byteCodeName; //Name of this ByteCode in string form, i.e.
    protected String sourceByteCode; //Used by Debug output


    
    public ByteCode(String bcName) {
        this.byteCodeName = bcName; //Standard constructor, just set the ByteCode's name
    }

    /**
     * Sets the original sourceByteCode, i.e. "LIT 1 m" 
     * @param in 
     */
    public void setSourceByteCode(String in)
    {
        sourceByteCode = in;
    }
    
    /**
     * Retrieves the original sourceByteCode, i.e. "LIT 1 m"
     * @return 
     */
    public String getSourceByteCode()
    {
        return sourceByteCode;
    }
    
    public String getByteCodeName()
    {
        return this.byteCodeName; //Return byteCodeName, used by Program to determine
                                  //if this is one of the ByteCode's that resolve a Label.
    }
    
    /**
     * Execute this ByteCode, overloaded in child class
     * @param vm
     */
    public abstract void execute(VirtualMachine vm);
    
    /**
     * Overloaded in each ByteCode, prints the associated debug information
     */
    public abstract String getDebugLine(VirtualMachine vm);
    
    public abstract void init(Vector<String> args);
}
