package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * READ ByteCode, takes 0 arguments
 * @author frank
 */
public class ReadCode extends ByteCode {

    public ReadCode() {
        super("ReadCode");
    }
    
    /**
     * Tell the VM to prompt the user for an integer value
     * then push the value on the top of the stack
     * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Tell the VM to prompt the user for a value
        int readVal = 0;
        readVal = vm.readInt();
        
        //Push the val on the top of the stack
        vm.pushStack(readVal);
    
    }
    
    /**
     * Standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "READ"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
        ;//No need to process any arguments.
    }
}
