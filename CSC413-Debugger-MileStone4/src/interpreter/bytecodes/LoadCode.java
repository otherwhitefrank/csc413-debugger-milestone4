package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * LOAD ByteCode takes upto 2 args
 * @author frank
 */
public class LoadCode extends ByteCode {

    private int offset; //Literal to "LOAD 0"
    private String comment; //Comment in "LOAD 0 m"
    private int numArgs; //Number of arguments stored
    
    public LoadCode() {
        super("LoadCode");
    }

    /**
     * Retrieve the offset and ask the VM to load the value at frame + offset to top 
     * of runStack
     *
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Tell vm to load offset ontop of stack
        vm.loadStackOffset(this.offset);

    }

    /**
     * Two forms of Debug output
     * 1 arg == "LOAD 0"
     * 2 arg == "LOAD 0 m  load m"
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {

        //If no ID present then print standard sourceByteCode
        //If an ID is present then print "LOAD 2 a     <load a>"
        if (this.numArgs == 1)
        {
            //In case "LOAD 0" simply print the original sourceByteCode
            return this.getSourceByteCode();
        }
        else
        {
            //Case "LOAD 0 m    <load m>"
            String formattedString = this.getSourceByteCode()
                    + "\t<load " + this.comment + ">";
            return formattedString;
        }
    }

    @Override
    public void init(Vector<String> args) {
        if (!args.isEmpty())
        {
            this.numArgs = args.size(); //Save number of arguments
            this.offset = Integer.parseInt(args.get(0)); //Parse offset
            if (args.size() == 2)
            {
                //Save comment
                this.comment = args.get(1);
            }
        }        
    }
}


