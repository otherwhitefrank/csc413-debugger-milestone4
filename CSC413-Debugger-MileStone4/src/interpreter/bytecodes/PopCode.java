package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * POP ByteCode takes 1 argument
 * @author frank
 */
public class PopCode extends ByteCode {

    protected int numToPop; //Number of spots to pop of frame
    
    public PopCode() {
        super("PopCode");
    }

    /**
     * Pop n levels off the runStack
    * @param vm 
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Pop n levels off the runStack
        for (int i = 0; i < this.numToPop; i++)
        {
            //Ignore the value
            vm.popStack();
        }
    }
    
    /**
     * Print standard Debug output
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "POP 2"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {
        if (!args.isEmpty())
        {
            //Parse number of items to pop of frame.
            this.numToPop = Integer.parseInt(args.get(0));
        }
    
    }
}
