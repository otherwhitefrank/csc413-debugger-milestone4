package interpreter.bytecodes;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * ARGS ByteCode, takes 1 argument
 * @author frank
 */
public class ArgsCode extends ByteCode {

    int numArgs;

    
    public ArgsCode() {
        super("ArgsCode");
    }
    
    /**
     * Tells the VM to create a new frame at the first arg offset
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Setup new frame with numArgs
        vm.newFrameAt(this.numArgs);
    }

    /**
     * Standard debug print of ByteCode
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //ARGS simple prints the original sourceByteCode
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {

        //Make sure we have >0 arguments
        if (args.size() != 0)
        {
            //Parse argument into Integer
            String firstArg = args.get(0);
            this.numArgs = Integer.parseInt(firstArg);
        }
    }
}
