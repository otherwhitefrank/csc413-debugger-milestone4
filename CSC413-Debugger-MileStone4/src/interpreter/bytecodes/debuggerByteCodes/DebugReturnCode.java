package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ReturnCode;
import interpreter.debugger.DebugVirtualMachine;

/**
 * DebugReturnCode extends ReturnCode to also remove the current environment record
 * @author frank
 */
public class DebugReturnCode extends ReturnCode {

    public DebugReturnCode() {
        this.byteCodeName = "DebugReturnCode";
    }

    @Override
    public void execute(VirtualMachine vm) {
        super.execute(vm);
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            debugVM.popEnvironmentRecord();

        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugReturn ");
            System.exit(1);
        }
    }
}
