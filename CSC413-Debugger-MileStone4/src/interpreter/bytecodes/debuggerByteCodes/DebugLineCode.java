/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugLineCode is a new ByteCode that indicates the current execution line
 * @author frank
 */
public class DebugLineCode extends ByteCode {

    public DebugLineCode() {
        super("DebugLineCode");
    }
    
    private int currLine; 

    @Override
    public void execute(VirtualMachine vm) {

        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            debugVM.setCurrentLine(currLine);

        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugLine " + currLine);
            System.exit(1);
        }
    }

    /**
     * Standard Debug output
     *
     * @param vm
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "LINE 4"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {

        if (!args.isEmpty())
        {
            //Store the first argument as label
            currLine = Integer.valueOf(args.get(0));
        }
    }
}
