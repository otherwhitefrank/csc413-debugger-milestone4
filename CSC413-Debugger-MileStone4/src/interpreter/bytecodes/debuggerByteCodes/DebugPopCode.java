package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.PopCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugPopCode extends the basic pop bytecode to also remove symbols from the current 
 * environment record
 * @author frank
 */
public class DebugPopCode extends PopCode {

    public DebugPopCode() {
        this.byteCodeName = "DebugPopCode";
    }

    @Override
    public void execute(VirtualMachine vm) {
        super.execute(vm);
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            debugVM.popSymbolTable(numToPop);

        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugPop " + numToPop);
            System.exit(1);
        }
    }
}
