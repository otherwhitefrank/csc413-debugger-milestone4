/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugFunctionCode is a new ByteCode that sets up the function name, start and end lines
 * in the current environment record
 * @author frank
 */
public class DebugFunctionCode extends ByteCode {

    private String funcName;
    private int startLine;
    private int stopLine;

    public DebugFunctionCode() {
        super("DebugFormalCode");
    }

    @Override
    public void execute(VirtualMachine vm) {

        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;

            debugVM.setFunction(funcName, startLine, stopLine);
            
            debugVM.unsetJustEnteredFunction(); //Unset the just entered function flag
                                                //This is used for checking whether to break or not.
                                                //We disable breaking till we have properly entered the function

        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugFormal " + funcName + " " + startLine + " " + stopLine);
            System.exit(1);
        }
    }

    /**
     * Standard Debug output
     *
     * @param vm
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "LABEL continue<<2>>"
        return this.getSourceByteCode();
    }

    @Override
    public void init(Vector<String> args) {

        if (!args.isEmpty())
        {
            //Store the first argument as label
            funcName = args.get(0);
            startLine = Integer.valueOf(args.get(1));
            stopLine = Integer.valueOf(args.get(2));
        }
    }

}
