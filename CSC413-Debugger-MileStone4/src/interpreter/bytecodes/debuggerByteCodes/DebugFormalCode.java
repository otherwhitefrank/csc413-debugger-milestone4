/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interpreter.bytecodes.debuggerByteCodes;

import interpreter.VirtualMachine;
import interpreter.bytecodes.ByteCode;
import interpreter.debugger.DebugVirtualMachine;
import java.util.Vector;

/**
 * DebugFormalCode is a new ByteCode that enters a formal at an offset into the
 * current environment record
 * @author frank
 */
public class DebugFormalCode extends ByteCode {
    
    private String symbol;
    private String value;
    
    public DebugFormalCode() {
        super("DebugFormalCode");
    }
    
    @Override
    public void execute(VirtualMachine vm) {
        
        try
        {
            //Cast vm upto DebugVM
            DebugVirtualMachine debugVM = (DebugVirtualMachine) vm;
            
            //Parse value into offset
            int offset = Integer.valueOf(this.value);
            
            //Enter the symbol in our envRecord
            debugVM.enterSymbolAtOffset(symbol, offset);
            
        } catch (NumberFormatException e)
        {
            System.out.println("***Error can't cast offset in DebugFormal " + this.symbol + " " + this.value);
            System.exit(1);
        }
    }

    /**
     * Standard Debug output
     *
     * @param vm
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //Print standard sourceByteCode, i.e. "LABEL continue<<2>>"
        return this.getSourceByteCode();
    }
    
    @Override
    public void init(Vector<String> args) {
        
        if (!args.isEmpty())
        {
            //Store the first argument as label
            this.symbol = args.get(0);
            this.value = args.get(1);
        }
    }
    
}
